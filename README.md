Silverstripe google maps module
===============================
---

**Maintainer Contacts**

*	Born In Idea [Ltd] (<silverstripe@idea.lt>)

**Require CMS version** Silverstripe 3.x

Installation Instructions
-------------------------

1. Place this directory to your root silverstripe installation directory.

2. Open your ``mysite/_config.php`` file and include this line:
	
		<?php
		// Google maps extensions
		ContactsPage::add_extension("GMapExtension");
	
	Where ``ContactsPage`` is your page to include Google Maps module. The module is will be available in your page
	with created new tab name.
	
3. Visit ``http://example.com/dev/build/?flush=all`` to build database table and flushing templates. After building create
	``ContactPage`` (for example) and you should see a tab **Google Maps** at the backend administration pages.
	Also set up this module at your administration settings, you should see a tab Google Maps fill 
	up what you asking for. (**But it's just for optional**).

4. Front-end settings for Google Maps implementations:

*	Open your ``Page`` (``ContactPage``) were you compiled extension for and add this script at your init method of controller.

		<?php
		// <-- Start Google Maps variables
		$GMapMarker = (substr($this->GMapMarker()->Link(), -strlen(".png")) === ".png") ? $this->GMapMarker()->Link() : '';

		$GoogleConfig = SiteConfig::current_site_config();
		$customVariables = array(
			"_gmap_lat" => (!empty($this->GMapLat)) ? $this->GMapLat : 0,
			"_gmap_lng" => (!empty($this->GMapLng)) ? $this->GMapLng : 0,
			"_gmap_zoom" => (!empty($this->GMapZoom)) ? $this->GMapZoom : 0,
			// "_gmap_heading" => (!empty($this->GMapHeading)) ? $this->GMapHeading : 0,
			// "_gmap_pitch" => (!empty($this->GMapPitch)) ? $this->GMapPitch : 0,
			"_gmap_marker" => (!empty($GMapMarker)) ? Director::AbsoluteURL($GMapMarker) : '',
			"_gmap_element" => "GMap_Map",
			"_gmap_api" => (!empty($GoogleConfig->GMapAPI)) ? $GoogleConfig->GMapAPI : '',
			"_gmap_balloon" => (!empty($this->GMapBalloon)) ? $this->GMapBalloon : ''
		);

		Requirements::javascriptTemplate(GMAPMODULE_PATH . '/javascript/GMap.min.js', $customVariables);
		// EOF Google Maps variable -->

*	Then open your template of ``ContactPage(.ss)`` and add this line:
		
		<% include GMap_Frontend %>

	Remember to flush template after that.

Known Issues
------------
[Issue Tracker](https://bitbucket.org/idea_lt/google-maps/issues?status=new&status=open)